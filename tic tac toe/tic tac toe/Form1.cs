﻿namespace tic_tac_toe
{
    public partial class Form1 : Form
    {
        char XorO = 'o';
        int movement = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, EventArgs e)
        {

            timer1.Stop();
            timer1.Start();
            Button bt = (Button)sender;
            bt.Enabled = false;
            bt.BackColor = Color.Gray;
            if (XorO == 'o')
            {
                bt.Text = "o";
                if ((b1.Text == b2.Text && b2.Text == b3.Text && b3.Text != "") ||
                    (b4.Text == b5.Text && b5.Text == b6.Text && b6.Text != "") ||
                    (b7.Text == b8.Text && b8.Text == b9.Text && b9.Text != "") ||
                    (b1.Text == b4.Text && b4.Text == b7.Text && b7.Text != "") ||
                    (b2.Text == b5.Text && b5.Text == b8.Text && b8.Text != "") ||
                    (b3.Text == b9.Text && b9.Text == b6.Text && b6.Text != "") ||
                    (b1.Text == b5.Text && b5.Text == b9.Text && b9.Text != "") ||
                    (b3.Text == b5.Text && b5.Text == b7.Text && b7.Text != "")
                    )
                {
                    MessageBox.Show($"Переможець {XorO.ToString().ToUpper()}!!!");
                    tableLayoutPanel1.Enabled = false;
                    timer1.Stop();
                }
                else if (movement == 8)
                {
                    MessageBox.Show("Нічия!");
                    timer1.Stop();
                }
                XorO = 'x';
                movement++;
            }

            else
            {
                bt.Text = "x";
                if ((b1.Text == b2.Text && b2.Text == b3.Text && b3.Text != "") ||
                  (b4.Text == b5.Text && b5.Text == b6.Text && b6.Text != "") ||
                  (b7.Text == b8.Text && b8.Text == b9.Text && b9.Text != "") ||
                  (b1.Text == b4.Text && b4.Text == b7.Text && b7.Text != "") ||
                  (b2.Text == b5.Text && b5.Text == b8.Text && b8.Text != "") ||
                  (b3.Text == b9.Text && b9.Text == b6.Text && b6.Text != "") ||
                  (b1.Text == b5.Text && b5.Text == b9.Text && b9.Text != "") ||
                  (b3.Text == b5.Text && b5.Text == b7.Text && b7.Text != "")
                  )
                {
                    MessageBox.Show($"Переможець {XorO.ToString().ToUpper()}!!!");
                    tableLayoutPanel1.Enabled = false;
                    timer1.Stop();
                }
                else if (movement == 8)
                {
                    MessageBox.Show("Нічия!");
                    timer1.Stop();
                }
                XorO = 'o';
                movement++;
            }
        }

        private void newGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            XorO = 'o';
            movement = 0;
            b1.Enabled = true; b1.Text = ""; b1.BackColor = Color.White;
            b2.Enabled = true; b2.Text = ""; b2.BackColor = Color.White;
            b3.Enabled = true; b3.Text = ""; b3.BackColor = Color.White;
            b4.Enabled = true; b4.Text = ""; b4.BackColor = Color.White;
            b5.Enabled = true; b5.Text = ""; b5.BackColor = Color.White;
            b6.Enabled = true; b6.Text = ""; b6.BackColor = Color.White;
            b7.Enabled = true; b7.Text = ""; b7.BackColor = Color.White;
            b8.Enabled = true; b8.Text = ""; b8.BackColor = Color.White;
            b9.Enabled = true; b9.Text = ""; b9.BackColor = Color.White;
            tableLayoutPanel1.Enabled = true;
        }

        private void infoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Гра хрестики-нолики\nКожному гравцю надаєся 5 секунд на хід\nГру починають нолики");
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            MessageBox.Show($"Гравець {XorO.ToString().ToUpper()} не встиг зробити свій хід!!! Ігра завершена");
            tableLayoutPanel1.Enabled = false;
            timer1.Stop();

        }
    }
}